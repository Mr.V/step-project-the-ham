$(document).ready(function() {

    $('.prev').click(function() {
        if ($('.activeSldImg').index() <= 1) {
            $('.activeSldImg').removeClass('activeSldImg');
            $($('.bordShad2')[3]).addClass('activeSldImg');

            $('.firstPrs.WsActive').removeClass('WsActive');
            $($('.firstPrs')[3]).addClass('WsActive');

        } else {
            $('.activeSldImg').removeClass('activeSldImg').prev().addClass('activeSldImg');
            $('.firstPrs.WsActive').removeClass('WsActive').prev().addClass('WsActive');
        }

    })
    $('.next').click(function() {
        if ($('.activeSldImg').index() >= 4) {
            $('.activeSldImg').removeClass('activeSldImg');
            $($('.bordShad2')[0]).addClass('activeSldImg');

            $('.firstPrs.WsActive').removeClass('WsActive');
            $($('.firstPrs')[0]).addClass('WsActive');

        } else {
            $('.activeSldImg').removeClass('activeSldImg').next().addClass('activeSldImg');
            $('.firstPrs.WsActive').removeClass('WsActive').next().addClass('WsActive');
        }
    });
    $('.bordShad2').click(function() {
        $('.bordShad2').removeClass('activeSldImg');
        $(this).addClass('activeSldImg');
        const indx = $(this).index();

        $('.firstPrs').removeClass('WsActive')
        $($('.firstPrs')[indx - 1]).addClass('WsActive')
    });
});