/**
 * Buttons animation hover
 */
$('.button-1, .button-2, .purchase-now').mouseover(function() {
    $(this).animate({
        'border-width': '4px'
    });
});

$('.button-1, .button-2, .purchase-now').mouseout(function() {
    $(this).animate({
        'border-width': '0px'
    });
});

/**
 * Section "Our Services"
 */
document.querySelector('.btn').classList.add('active');
document.querySelector('.txt').classList.add('active');

document.querySelectorAll('.btn').forEach(el => {
    el.addEventListener('click', selectFirstTabs);
});

function selectFirstTabs(event) {
    let target = event.target.dataset.target;

    if (target.tagName == 'SPAN') return;

    document.querySelectorAll('.btn, .txt').forEach(el => {
        el.classList.remove('active');
    });

    event.target.classList.add('active');
    document.querySelector('.' + target).classList.add('active');
}

/**
 * Section "Our Amazing Work" 
 */
let ourMenu = document.querySelector('.our-menu').classList.add('active');
let images = document.querySelector('.images').classList.add('active');

document.querySelectorAll('.our-menu').forEach(el => {
    el.addEventListener('click', selectTabs);
});

function selectTabs(event) {
    let target = event.target.dataset.target;

    if (target.tagName == 'SPAN') return;

    document.querySelectorAll('.our-menu, .images').forEach(el => {
        el.classList.remove('active');
    });

    event.target.classList.add('active');
    document.querySelector('.' + target).classList.add('active');
}

/**
 * Section "Our Amazing Work" content load
 */
$('.load').click(function() {
    $('.load').hide(1000)

    $('#floatingBarsG').show(1000)

    setTimeout(() => {
        $('.load').show(1000);

        $('#floatingBarsG').hide(1000);
    }, 3000)
});